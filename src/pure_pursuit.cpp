#include "mkhlib/pathFollowing/purePursuit/pure_pursuit.h"

void mkhlib::Angle::SetValue(QAngle value)
{
  _value = value;

  if(_value >= 360.0_deg)
  {
    SetValue(_value - (floorf((_value.convert(1_deg) / 360.0)) * 360_deg));
  }

  if(_value < 0_deg)
  {
    SetValue(360_deg + _value - (ceilf((_value.convert(1_deg) / 360.0)) * 360_deg));
  }

}

QAngle mkhlib::Angle::ShortestError(Angle from, Angle to) // returns shortest error from angle this method is being called on to other
{
  if(to.GetValue() - from.GetValue() <= 180_deg && to.GetValue() - from.GetValue() >= -180_deg)
  {
    return to.GetValue() - from.GetValue();
  }
  else //need to go the other way around
  {
    if(from.GetValue() > 180_deg)
    {
      return 360_deg - from.GetValue() + to.GetValue();
    }
    else 
    {
      return -from.GetValue() + to.GetValue() - 360_deg;
    }
    
  }
}

QLength mkhlib::Pose::Distance(Pose pose1, Pose pose2)
{
  QLength xError = pose2.x - pose1.x;
  QLength yError = pose2.y - pose1.y;
  return sqrtf(((xError * xError) + (yError * yError)).convert(1_m * 1_m)) * 1_m;
}

QLength mkhlib::Pose::Distance(Pose pose1, OdomState pose2)
{
  QLength xError = pose2.x - pose1.x;
  QLength yError = pose2.y - pose1.y;
  return sqrtf(((xError * xError) + (yError * yError)).convert(1_m * 1_m)) * 1_m;
}

QLength mkhlib::Pose::Distance(Pose pose, Point point)
{
  QLength xError = point.x - pose.x;
  QLength yError = point.y - pose.y;
  return sqrtf(((xError * xError) + (yError * yError)).convert(1_m * 1_m)) * 1_m;
}


bool mkhlib::PurePursuit::Path::followPath(std::shared_ptr<OdomChassisController> chassis)
{
  if(!_pathVerified)
  {
    verifyPath();
  }
  return true;
}

// float mkhlib::PurePursuit::Path::calculateCurvature(Pose point1, Pose point2, Pose point3)
// {
//   float x1 = point1.x.convert(1_m);
//   float x2 = point2.x.convert(1_m);
//   float x3 = point3.x.convert(1_m);
//   float y1 = point1.y.convert(1_m);
//   float y2 = point2.y.convert(1_m);
//   float y3 = point3.y.convert(1_m);
//   float x12 = x1 - x2;
//   float x13 = x1 - x3;
  
//   float y12 = x1 - x2;
//   float y13 = y1 - y3;

//   float y31 = y3 - y1;
//   float y21 = y2 - y1;

//   float x31 = x3 - x1;
//   float x21 = x2 - x1;

//   // x1^2 - x3^2
//   float sx13 = powf(x1, 2) - powf(x3, 2);
//   float sy13 = powf(y1, 2) - powf(y3, 2);
 
//   float sx21 = powf(x2, 2) - powf(x1, 2);
//   float sy21 = powf(y2, 2) - powf(y1, 2);

//   int f = ((sx13) * (x12) + (sy13) * (x12) + (sx21) * (x13) + (sy21) * (x13)) / (2 * ((y31) * (x12) - (y21) * (x13)));

//   int g = ((sx13) * (y12) + (sy13) * (y12) + (sx21) * (y13) + (sy21) * (y13)) / (2 * ((x31) * (y12) - (x21) * (y13)));

//   int c = -pow(x1, 2) - pow(y1, 2) - 2 * g * x1 - 2 * f * y1;

//   int sqr_of_r = -g * -g + -f * -f - c;

//   return sqrtf(sqr_of_r);
// }

void mkhlib::PurePursuit::Path::verifyPath()
{
    QLength distance = 0_m;
    float curvature = 0;

    for(int i = 0; i < pointList.size(); i++)
    {
      if(i > 0)
      {
        distance = distance + Pose::Distance(pointList[i].pose, pointList[i-1].pose);
        if(i < pointList.size() - 1)
        {
          // curvature = radius of circle that intersects the current point and two points on either side, curvature = 1 /radius
          //curvature = 1 / calculateCurvature(pointList[i-1].pose, pointList[i].pose, pointList[i + 1].pose);

        }
      }
      if(i == 0)
      {
        distance = 0_m;
      }
      
      pointList[i].distance = distance;
      pointList[i].curvature = curvature;
      
    }
    //calculate velocities backwards
    pointList[pointList.size() - 1].targetVelocity = 0;
    for(int i = pointList.size() - 2; i >= 0; i--)
    {
      float pointDistance = Pose::Distance(pointList[i + 1].pose, pointList[i].pose).convert(1_m);
      //float newVelocity = MIN()
    }
}

bool mkhlib::PurePursuit::SimplePath::followPath(std::shared_ptr<OdomChassisController> chassis, float trackWidth)
{
  PathPoint closestPoint = pointList[0];
  int closestIndex = 0;
  QLength closestDistance = Pose::Distance(pointList[0].pose, chassis->getState());
  float tLookaheadPoint = 0;
  Pose lookaheadPoint = pointList[0].pose;
  int lookaheadSegmentStartIndex = 0;
  while(true)
  {
    for(int i = closestIndex; i < pointList.size(); i++) //find closest point, used for finding velocity
    {
      QLength distance = Pose::Distance(pointList[0].pose, chassis->getState());
      if(distance < closestDistance)
      {
        closestPoint = pointList[i];
        closestIndex = i;
        closestDistance = distance;
      }
    }

    for(int i = lookaheadSegmentStartIndex; i < pointList.size() - 1; i++) //iterate through line segments to find closest intersection
    {
      float tIntersection = 0;
      bool intersectionFound = false;

      Pose startingSegment = pointList[i].pose;
      Pose endingSegment = pointList[i + 1].pose;
      Pose chassisPosition = Pose(chassis->getState());
      Point lineVector = {endingSegment.x - startingSegment.x, endingSegment.y - startingSegment.y};
      QLength lineMagnitude = (lineVector.x * lineVector.x + lineVector.y * lineVector.y).sqrt();
      lineVector.x = lineVector.x / lineMagnitude * 1_m;
      lineVector.y = lineVector.y / lineMagnitude * 1_m;
      Point chassisToStart = {startingSegment.x - chassisPosition.x, startingSegment.y - chassisPosition.y};

      QArea a = lineVector.x * lineVector.x + lineVector.y * lineVector.y;
      QArea b = 2 * (chassisToStart.x * lineVector.x + chassisToStart.y * lineVector.y);
      QArea c = (chassisToStart.x * chassisToStart.x + chassisToStart.y * chassisToStart.y) - _lookahead * _lookahead;
      float discriminant = b.convert(1_m * 1_m) * b.convert(1_m * 1_m) - 4 * a.convert(1_m * 1_m) * c.convert(1_m * 1_m); // in m^4

      if(discriminant < 0)
      {
        //no intersection
      }
      else
      {
        float discriminantSqrt = sqrtf(discriminant); // in m^2
        float t1 = (-b.convert(1_m * 1_m) - discriminantSqrt) / (2 * a.convert(1_m * 1_m));
        float t2 = (-b.convert(1_m * 1_m) + discriminantSqrt) / (2 * a.convert(1_m * 1_m));

        if(t1 >= 0 && t1 <= 1)
        {
          intersectionFound = true;
          tIntersection = t1;
          // t1 intersection!
        }
        if(t2 >= 0 && t2 <= 1)
        {
          intersectionFound = true;
          tIntersection = t2;
          // t2 intersection!
        }
        //no intersection
      }
      if(intersectionFound)
      {
        Point ray = {tIntersection * lineVector.x, tIntersection * lineVector.y};
        Pose intersectionPoint = startingSegment + ray;
        if(tIntersection > tLookaheadPoint)
        {
          //fractional index good to go!
          tLookaheadPoint = tIntersection;
          lookaheadPoint = intersectionPoint;
          lookaheadSegmentStartIndex = i;
          break;
        }
      }


    }

    //bang bang bang! found lookahead, time to calculate curvature

    OdomState chassisState = chassis->getState();

    float a = -tanf(chassisState.theta.convert(1_rad)); // radians
    float b = 1;
    float c = tanf(chassis->getState().theta.convert(1_rad)) * (chassisState.x - chassisState.y).convert(1_m); //in meters

    QLength xDist = (fabsf(a * lookaheadPoint.x.convert(1_m) + b * lookaheadPoint.y.convert(1_m) + c) / sqrtf(a * a + b * b)) * 1_m;

    float unsignedCurvature = 2 * xDist.convert(1_m) / (_lookahead.convert(1_m) * _lookahead.convert(1_m));
    // simply the cross product between robot line and robot to lookahead
    int sign = sgn<float>(sinf(chassisState.theta.convert(1_rad)) * (lookaheadPoint.x - chassisState.x).convert(1_m) - cosf(chassisState.theta.convert(1_rad)) * (lookaheadPoint.y - chassisState.y).convert(1_m));
    float signedCurvature = unsignedCurvature * sign;


    //control wheels to go towards the curvature
    float tgtVelocity = 200; //rpm
    float left = tgtVelocity * (2 + signedCurvature * trackWidth) / 2;
    float right = tgtVelocity * (2 - signedCurvature * trackWidth) / 2;

    chassis->getModel()->left(left);
    chassis->getModel()->right(right);

  }
  return false;
}

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}
