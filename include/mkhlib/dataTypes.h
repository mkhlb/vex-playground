#include "api.h"
#include "okapi/api.hpp"

using namespace okapi;

namespace mkhlib
{
    class Vector2
    {
        public:
            QLength x;
            QLength y;
            QLength GetMagnitude();
            QAngle GetAngle();
    };

    class Angle
    {
        public:
            Angle()
            {
                SetValue(0_deg);
            }
            Angle(QAngle initAngle)
            {
                SetValue(initAngle);
            }
            void SetValue(QAngle value);
            QAngle GetValue() {return _value;}
            static QAngle ShortestError(Angle from, Angle to);
        private:
            QAngle _value;
    };
};