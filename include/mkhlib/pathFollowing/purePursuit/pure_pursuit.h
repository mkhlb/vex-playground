#include "api.h"
#include "okapi/api.hpp"

using namespace okapi;
namespace mkhlib
{
    namespace PurePursuit
    {
        class PathPoint
        {
            public:
                PathPoint()
                {
                    pose = Pose();
                }
                PathPoint(QLength x, QLength y, QAngle w)
                {
                    pose = Pose(x, y, w);
                }

                PathPoint operator - (PathPoint const &obj)
                {
                    return PathPoint(pose.x - obj.pose.x, pose.y - obj.pose.y, pose.w.GetValue());
                }

                Pose pose; // set in constructor
                float curvature; // set based on other points
                float targetVelocity; // set based on other points
                QLength distance; // set based on other points
                

        };

        class Path
        {
            public:
                Path(std::vector<PurePursuit::PathPoint> points, float maximumVelocity = 200, float maximumAcceleration = 200) // max velocity in rpm, max accel in rpm per second
                {
                    pointList = std::vector<PurePursuit::PathPoint>(points);
                    this->maximumVelocity = maximumVelocity;
                    this->maximumAcceleration = maximumAcceleration;
                }
                std::vector<PurePursuit::PathPoint> pointList;
                float maximumVelocity;
                float maximumAcceleration;
                float k = 2.5; // constant around 1 - 5 based on how slow you want to go around turns
                void smoothPoints();
                void injectPoints();
                void verifyPath();
                bool followPath(std::shared_ptr<OdomChassisController> chassis); // return based on if path is completed
                //float calculateCurvature(Pose point1, Pose point2, Pose point3);
            private:
                bool _pathVerified = false;
                
        };

        class SimplePath
        {
            public:
                SimplePath(std::vector<PurePursuit::PathPoint> points, QLength lookahead = 2_m) // max velocity in rpm, max accel in rpm per second
                {
                    pointList = std::vector<PurePursuit::PathPoint>(points);
                    _lookahead = lookahead;
                }
                std::vector<PurePursuit::PathPoint> pointList;
                bool followPath(std::shared_ptr<OdomChassisController> chassis, float trackWidth); // return based on if path is completed
            private:
                QLength _lookahead;
        };
    };
    class Pose
    {
        public:
            QLength x;
            QLength y;
            Angle w;

            Pose()
            {
                x = 0_m;
                y = 0_m;
                w = 0_rad;
            }
            Pose(Point p)
            {
                x = p.x;
                y = p.y;
                w = Angle(0_deg);
            }
            Pose(OdomState s)
            {
                x = s.x;
                y = s.y;
                w = Angle(s.theta);
            }
            Pose(QLength initX,QLength initY,QAngle initW)
            {
                x = initX;
                y = initY;
                w = Angle(initW);
            }
            Pose(QLength x, QLength y)
            {
                this->x = x;
                this->y = y;
                w = Angle(0_deg);
            }

            Pose operator + (Point const &obj)
            {
                return Pose(x - obj.x, y - obj.y, w.GetValue());
            }

            static QLength Distance(Pose pose1, Pose pose2);
            static QLength Distance(Pose pose1, OdomState pose2);
            static QLength Distance(Pose pose, Point point);
        
    };

    
};